<?php

class Role implements \PHPIAC\RoleInterface
{
    public function __invoke(array $config): array
    {
        $config = array_replace_recursive([
            'package' => 'zsh',
        ], $config);

        return [
            (new \PHPIAC\Task())->setModule(new \PHPIAC\Modules\AptModule([
                'package' => $config['package'],
            ])),
        ];
    }
}
