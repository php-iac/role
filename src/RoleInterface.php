<?php

namespace PHPIAC\Role;

interface RoleInterface
{
    /**
     * @param array $config
     *
     * @return Task[]
     */
    public function __invoke(array $config = []): array;
}
